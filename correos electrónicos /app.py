from flask import Flask, render_template, request
from celery import Celery
from flask_mail import Mail, Message

app = Flask(__name__)

# Configuración de Flask-Mail
app.config['MAIL_SERVER'] = 'smtp.example.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USERNAME'] = 'your_username'
app.config['MAIL_PASSWORD'] = 'your_password'

# Configuración de Celery
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/1'

mail = Mail(app)
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

@celery.task
def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    mail.send(msg)

@app.route('/')
def index():
    return '¡Bienvenido a la aplicación Flask con Celery!'

@app.route('/send_email', methods=['GET', 'POST'])
def send_email_route():
    if request.method == 'POST':
        recipients = ['recipient@example.com']
        subject = 'Asunto'
        sender = 'sender@example.com'
        text_body = 'Este es el cuerpo del correo en texto plano'
        html_body = '<p>Este es el cuerpo del correo en formato HTML</p>'
        
        send_email.delay(subject, sender, recipients, text_body, html_body)
        
        return '¡Correo electrónico enviado!'
    
    return render_template('send_email.html')

if __name__ == '__main__':
    app.run()
